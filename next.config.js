/** @type {import('next').NextConfig} */

const isProd = process.env.NODE_ENV === 'production'

const nextConfig = {
    assetPrefix: isProd ? '/bron-portfolio-website/' : '/',
    trailingSlash: true,
    output: 'export',
}


module.exports = nextConfig
